#include <stdio.h>
#include <stdlib.h>
#include "include/hebrew.h"

const UChar letters[] = { 
	0x05D0, // א
	0x05D1, // ב
	0x05D2, // ג
	0x05D3, // ד
	0x05D4, // ה
	0x05D5, // ו
	0x05D6, // ז
	0x05D7, // ח
	0x05D8, // ט
	0x05D9, // י
	0x05DB, // כ
	0x05DC, // ל
	0x05DE, // מ
	0x05E0, // נ
	0X05E1, // ס
	0X05E2, // ע
	0X05E4, // פ
	0X05E6, // צ
	0X05E7, // ק
	0X05E8, // ר
	0X05E9, // ש
	0X05EA  // ת
};


/**
 * @brief Get the letter index of a hebrew letter in the hebrew alphabet.
 * NOTE: does not include dugix or endy letters
 * 
 * @param letter 
 * @return uint8_t 
 */
uint8_t get_letter_index(UChar letter) {
	switch (letter) {
		case 0x05DA: return 10; // ך
		case 0x05DD: return 13; // ם
		case 0x05DF: return 14; // ן
		case 0x05E3: return 17; // ף
		case 0x05E5: return 19; // ץ
	}
	for (uint8_t i = 0; i < LETTERS; ++i) {
		if (letters[i] == letter) {
			return i;
		}
	}
	return UINT8_MAX;
}


/**
 * @brief get the letter an index represents.
 */
UChar get_index_letter(uint8_t index) {
	if (index <= 0 || index > LETTERS)
		u_printf("Invalid index %i\n", index);
	return letters[index-1];
}

/**
 * swap the last character of a word with its mncpq version
 */
void mncpq(UChar word[]) {
	UChar last = word[u_strlen(word)-1];

	switch (last) {
		case 0x05DB: last = 0x05DA; break; // ך
		case 0x05DD: last = 0x05DD; break; // ם
		case 0x05E0: last = 0x05DF; break; // ן
		case 0x05E4: last = 0x05E3; break; // ף
		case 0x05E6: last = 0x05E5; break; // ץ
	}

	word[u_strlen(word)-1] = last;
}
