#include "include/hebrew.h"
#include "include/trie.h"
#include <stdio.h>
#include <string.h>

TrieNode* root;
uint8_t number_len = 0;
UChar word[128];
UChar permutations[128][4];

void calculate_permutations(uint8_t pos) {
	// printf("%i\n", pos);
	if (pos == number_len) {
		word[pos+1] = '\0';
		if (trie_search(root, word)) {
			mncpq(word);
			u_printf("%S\n", word);
		}
	}
	
	for (uint8_t i = 0; permutations[pos][i] != '\0'; i++) {
		word[pos] = permutations[pos][i];
		calculate_permutations(pos + 1);
	}
}

int main(int argc, char* argv[]) {
	if (argc != 2)
		exit(EXIT_FAILURE);

	char filename[] = "he_IL.dic";
	FILE* file = fopen(filename, "r");

	if (file != NULL) {
		root = trienode_make('\0');
		char aline[20]; // longest line is 18 characters long
		while(fgets(aline, sizeof aline, file) != NULL) {
			UChar line[20];
			u_uastrcpy(line, aline);
			trie_insert(root, line);
		}
		fclose(file);

		char number[128];
		strcpy(number, argv[1]);
		number_len = strlen(number);
		// calculate list of possible words that we can possibly make
		for (uint8_t i=0; number[i] != '\0'; ++i) {
			uint8_t j = 0;
			permutations[i][j++] = get_index_letter(number[i]-'0');
			switch (get_index_letter(number[i]-'0')) {
			case 0x05D0:
				permutations[i][j++] = 0x05D9;
				permutations[i][j++] = 0x05E7;
				break;
			case 0x05d1:
				permutations[i][j++] = 0x05db;
				permutations[i][j++] = 0x05e8;
				break;
			case 0x05d2:
				permutations[i][j++] = 0x05dc;
				permutations[i][j++] = 0x05e9;
				break;
			case 0x05d3:
				permutations[i][j++] = 0x05de;
				permutations[i][j++] = 0x05ea;
				break;
			case 0x05d4:
				permutations[i][j++] = 0x05e0;
				break;
			case 0x05d5:
				permutations[i][j++] = 0x05e1;
				break;
			case 0x05d6:
				permutations[i][j++] = 0x05e2;
				break;
			case 0x05d7:
				permutations[i][j++] = 0x05e4;
				break;
			case 0x05d8:
				permutations[i][j++] = 0x05e6;
				break;
			} 
			permutations[i][j] = '\0';
		}

		// for each permutation, find if such a word exists
		// for (uint8_t i=0; i < number_len; ++i) {
			calculate_permutations(0);
		// }
		trienode_free(root);
	} else {
		perror(filename);
	}

	return EXIT_SUCCESS;
}