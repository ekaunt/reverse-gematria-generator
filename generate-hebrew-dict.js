const fs = require("fs");

// instantiate our trie
const trie = {};
function insert(word) {
  let node = trie;
  for (const letter of word) {
    if (!node[letter]) {
      node[letter] = {};
    }
    node = node[letter];
  }
  node.end = true;
}

const he_dic = fs
  .readFileSync("he_IL.dic", { encoding: "utf-8" })
  .toString()
  .split("\r\n");

he_dic.forEach((word) => insert(word));

fs.writeFileSync("site/hebrew-dict.js", `const trie=${JSON.stringify(trie)}`, {
  encoding: "utf-8",
});
