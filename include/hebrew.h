#ifndef GYMATRIIY_HEBREW
#define GYMATRIIY_HEBREW
#include <unicode/uchar.h>

#define LETTERS 22

uint8_t get_letter_index(UChar letter);
UChar get_index_letter(uint8_t index);
void mncpq(UChar word[]);
#endif