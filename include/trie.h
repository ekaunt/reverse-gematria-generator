#ifndef GYMATRIIY_TRIE
#define GYMATRIIY_TRIE
#include <stdbool.h>
#include <stdlib.h>
#include <unicode/uchar.h>
#include "hebrew.h"

typedef struct TrieNode TrieNode;
struct TrieNode {
	UChar letter;
	TrieNode* children[LETTERS];
	bool is_leaf;
};

TrieNode* trienode_make(UChar letter);
void trienode_free(TrieNode* node);
void trie_insert(TrieNode* root, UChar word[]);
bool trie_search(TrieNode* root, UChar word[]);
#endif