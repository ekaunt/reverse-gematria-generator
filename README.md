# Reverse Gematria Generator

Input a mispar qatan, and out comes a list of words that match!

Find all words that a given gematria number (מספר קטן) is equal to.

[Try it out online now!](https://ekaunt.gitlab.io/reverse-gematria-generator/)

## Foreword

This program generates real words based on a number, so שמע gets generated from 347. It's different than regular gematria becuase שמע ≠ 347 (its 410). This program processes each letter separately.

```
עמש
↓↓↓ 
347
```

This is very useful for trying to come up with mnemonics for lock codes.

## Installation

You'll need to have Unicode support on your computer

Run 
```bash
git clone https://gitlab.com/ekaunt/reverse-gematria-generator
cd reverse-gematria-generator
make
```

## Use

To use all you need to do is run the program and give it as an argument the number you're trying to get a gematria of. The output is a list of all possible words that can be created based on the input number, each on a new line.

#### Examples:
```bash
> ./gymatriiy 152
אהב
אנך
יהב
קנך
> ./gymatriiy 347 
גדע
גמע
שמע
```
