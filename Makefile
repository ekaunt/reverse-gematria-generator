CC=gcc
CFLAGS=-g
HEADER=include
OBJ=obj
SOURCES=$(wildcard *.c)
OBJECTS=$(patsubst %.c, $(OBJ)/%.o, $(SOURCES))
CFLAGS=-g
PKG_CONFIG=`pkg-config --cflags --libs icu-uc icu-io`

gymatriiy: $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(PKG_CONFIG)

$(OBJ)/%.o: %.c $(HEADER)/%.h
	$(CC) $(CFLAGS) -I$(HEADER) -c $< -o $@

$(OBJ)/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm gymatriiy $(OBJ)/*.o;