#include "include/trie.h"

TrieNode* trienode_make(UChar letter) {
	TrieNode* node = (TrieNode*) calloc(1, sizeof (TrieNode));
	for (uint8_t i=0; i < LETTERS; ++i)
		node->children[i] = NULL;
	node->is_leaf = false;
	node->letter = letter;
	return node;
}

void trienode_free(TrieNode* node) {
	for (uint8_t i=0; i<LETTERS; ++i) {
		if (node->children[i])
			trienode_free(node->children[i]);
		else
			continue;
	}
	free(node);
}

void trie_insert(TrieNode* root, UChar word[]) {
	TrieNode* temp = root;
	for(uint8_t i=0; word[i]!='\0'&&word[i]!='\n'; ++i) {
		uint8_t idx = get_letter_index(word[i]);
		if (idx == UINT8_MAX)
			continue;
		if(temp->children[idx] == NULL) {
			temp->children[idx] = trienode_make(word[i]);
		} else {
			// do nothing. the node already exists
		}
		temp = temp->children[idx];
	}
	temp->is_leaf = true;
}

bool trie_search(TrieNode* root, UChar word[]) {
	TrieNode* temp = root;
	for(uint8_t i=0; word[i]!='\0'; ++i) {
		uint8_t pos = get_letter_index(word[i]);
		if (pos == UINT8_MAX)
			continue;
		if (temp->children[pos] == NULL)
			return false;
		temp = temp->children[pos];
	}
	if (temp != NULL && temp->is_leaf == true)
		return true;
	return false;
}